Tüketici fiyat endeksi; adı üstünde, biz tüketicilerin markete gittiğimizde karşılaştığı fiyat değişimlerini gösterir.

Üretici fiyat endeksi ise enflasyonun üreticilere yansıyan yönünü gösterir. Çalışan maaşları, elektrik, doğalgaz ve petrol gibi enerji maliyetleri ve hammadde fiyatları gibi kalemlerdeki fiyat değişimlerini gösterir.
Sun Dec 25 2022 21:56:37 GMT+0300 (GMT+03:00)
