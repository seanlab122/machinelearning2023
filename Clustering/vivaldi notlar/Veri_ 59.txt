Veri ölçeklendirme
robustscaler: verileri kantil aralığına göre ölçeklendirir,aykırı değerlerle daha iyi çalışır,
medyan değeri sonra kullanılmak üzere elenir
birçok yapay öğrenme modeli için ortak gereksinimdir
X-Q1(X) / Q3(X)-Q1(X)

MinMaxScaling: veriyi 0 ve 1 aralığında ölçeklendirir, aykırı değerlerin fazla olduğu veri setlerinde iyi çalışamayabilir
X-Xmin / Xmax-Xmin

MaxAbsScaler: her özelliğin maksimum mutlak değeri 1 olacak şekilde veriler ölçeklenir
X / max(abs(X))

Standardizasyon: ortalama değerin 0, standart sapmanın ise 1 değerini aldığı dağılımın normale yaklaştığı metottur
(X - u) / s

PowerTransFormer: varyansı stabilize etmek ve çarpıklığı en aza indirmek için en uygun ölçeklendirme faktörünü bulur



Sat Feb 05 2022 14:52:24 GMT+0300 (GMT+03:00)
