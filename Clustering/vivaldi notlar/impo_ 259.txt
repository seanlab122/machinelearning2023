import requests
from bs4 import BeautifulSoup
import googletrans
from googletrans import Translator

# İndirilecek web sitesi URL'si
url = "https://www.example.com"

# Web sitesini indirin ve HTML kodunu Beautiful Soup ile işleyin
response = requests.get(url)
soup = BeautifulSoup(response.content, 'html.parser')

# Google Translate API'sini kullanarak tüm metinleri Hintçe'ye çevirin
translator = Translator()
for element in soup.find_all():
    if element.string:
        translation = translator.translate(element.string, src='en', dest='hi')
        element.string = translation.text

# Çeviriyi doğrudan HTML koduna entegre edin
translated_html = soup.prettify()

# Çevirilmiş HTML kodunu dosyaya yazdırın veya kullanın
print(translated_html)
Thu Mar 02 2023 14:43:13 GMT+0300 (GMT+03:00)
https://chat.openai.com/chat