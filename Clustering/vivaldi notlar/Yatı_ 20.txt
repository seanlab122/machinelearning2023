Yatırım yapmak için iki önemli araç
* Hisse senedi: şirkette belli bir mülkiyet payını temsil eder
* Borsa yatırım fonu: Karışık meyve tabağı gibi düşünebilirsin. Bir çok sayıda hisse senedine, değerli madene, emtia veya sektöre aynı anda yatırım yapabilmeni sağlar. Örneğin, elektrikli araç sektörü yatırım fonunu alırsan, sektörde bulunan onlarca hisse senedine tek bir fon aracılığıyla kolayca yatırım yapmış olursun


Sun Dec 25 2022 21:29:14 GMT+0300 (GMT+03:00)
