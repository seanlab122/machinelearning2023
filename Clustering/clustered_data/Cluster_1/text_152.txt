Veri aşırı örnekleme
imblearnover_sampling
SMOTE yapay azınlık aşırı örnekleyici  
SAMPLING_STRATEGY Float olduğunda yeniden örneklemeden sonra azınlık sınıfındaki örnek sayısının çoğunluk sınıfındaki örnek sayısına göre istenen oranına karşılık gelir
NOT float yalnızca ikili sınıflandırma için kullanılabilir Çok sınıflı sınıflandırma için bir hata oluştu
str olduğunda yeniden örnekleme tarafından hedeflenen sınıfı belirtin Farklı sınıflardaki örnek sayıları eşitlenecektir
minority resample only the minority class
not minority resample all classes but the minority class
not majority resample all classes but the majority class
all resample all classes
auto equivalent to not majority
Dict olduğunda anahtarlar hedeflenen sınıflara karşılık gelir Değerler hedeflenen her sınıf için istenen örnek sayısına karşılık gelir
RANDOM_STATE int olursa verilen değer kadar karıştırılır böylece her çalıştırdığımızda sabit ve karışmış veri elde ederiz
K_NEIGHBORS 
int ise yapay örnekleri oluşturmak için kullanılacak en yakın komşu sayısı
KneighborMixin de kalıtılan bir estimatörtahminci ise k_neighborsu bulacaktır
n_jobs int değer alır
varsayılan  dir  olursa tüm çekirdekler kullanılır
SMOTENC
SMOTEN
BorderLineSMOTE
SVMSMOTE
ADASYN
KMeansSMOTE
Sat Feb    GMT GMT